---
layout: post
title: "Notes"
published: true
---

* 16 November 2020 at 7:30pm

** Location
 - Jitsi

** Attendees
- Present
    - Alan Heighway (board member)
    - Stewart Russell (board member)
    - Alex Volkov (board member)

    - Scott Sullivan (board member)
    - Gordon Chillcott
- Absent
    - Warren McPherson (board member)

** Topics

** Items from Previous Meetings
- [] Scott to Contact bank and start Signing Auth updates.
  - Inprogress, old bank contact has moved on.
- [] Alex to Contact Linode about transfering the account and services.
  - Inprogress.
- [] Scott to update mailman configs for missing users.
  - Was lacking access to severs (now fixed).
- [-] Scott to Contact Mike Hoye about December.
  - We have Mike!
 - [ ] Update _data/members.yml after board election, each year
 - [-] Alex [1/3]
   - [-] Contacting Mike Hoye about possibly presenting in November or December
     - Scott Contacted and has confirmed Mike for December.
   - [ ] Publish the Ansible rules for spinning up Jitsi
 - [-] Alan
   - [-] Submit a brief presentation on implications of the LPI Community Partnership
   - [-] Present at December meeting
     - Prsentation was delivered by Evan L. at November meeting.
 - [ ] Chris
   - [ ] Ensure that we have record that we need one of this year's
     Board members to end their term October 2021
   - [ ] Visit Spadina/College CIBC to see requirements for Evan and
     Gord to become cosigners
     - Task reassigned to Scott S.
 - [-] Stewart
   - [-] Trevor Woerner (OpenEmbedded - he spoke before, yes?)
     recently gave a great talk on building embedded Linuxes if you
     wanted another speaker, Trevor Woerner (OpenEmbedded - he spoke
     before, yes?)  recently gave a great talk on building embedded
     Linuxes
     - He is avaliable, targeting January.
  
*** COVID-19 Reaction
  - With retrograde progress in the province, there will be no change in our current stance.

*** Next Ops Meeting
  - December date would be December 14th, 7:30pm
  - Online vi Jitsi

*** Upcoming Meetings
 - Zoom vs Jitsi
   - Jitsi continues to improve, but it's one-many presenation experiance is lacking.
   - We wish to capture as high-quality video (with video of Mike).
   - Board has consensus to use Zoom for Mike's Meeting.
 
 - Should formalize the intros at the start of the meeting a little bit more
   - Alan to make said slide.

 - It doesn't have to be super formal, just we need an intentional process
 - The July meeting started with asking everyone who they were, and that worked pretty well
 - Various groups that are meeting virtually are losing attendees and
   interest because parts of the social interaction are lost
   - Alan to MC the December Meeting.


**** Generic Topics
  - Series on privacy topics (Warren as a key person)
    - Warren not here to elborate. Skipped.
  - Mailing list moderation and CoC
    - No known issue to address. Skipped.
  - Universal Access talk from Gord
    - This is a at the ready talk, either for a full session or mixed with other smaller talks. Gord to notife Speaker Coordinator when ready.

*** LPI Community Partnership
 - We would need to indicate a contact designate to be signer of the
   contract
 - Anticipated process for approval
   - get the contract (alan)
   - forward to the board
   - discuss at November
   - if all seems very favorable, a aye/nay would be next month
- Alan: Copy of the Contract was sent out for Review.
- Alan: This is to formalize our relationship and carries no obligations.
- Scott: So no-one else has read this contract? (Answer is NO)
- Scott: Okay, at least two other board members must read it and be ready to sign for December.

** TODO
- [] Alex to send Scott most recent bank statement.
- [] Make Logo Splace for Meeting the opening. - Alan
- [] Read LPI contract with goal of Signing at December Board meeting.