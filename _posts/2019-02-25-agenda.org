---
layout: post
title: "Agenda"
published: true
---

* 25 February 2018 at 7:30pm

** Location

- Zoom

** Agenda Topics
 - Timing/Location of next Ops meeting
 - Upcoming Meetings
 - ICANN material
 - Meetup.com action
 - Discuss Hugh's email (in operations list) and figure out possible meeting topics we can organize based on it.
 - Update wording of CoC that it applies to all GTALUG functions:
   - Dinner before the meeting
   - Beer after the meeting
   - Special Events / BBQ
 - Linode, is the new toronto region up yet? How painful to migrate server to it?
 - How about Hetzner (Germany/Finland)? -- https://www.hetzner.com/cloud
   - Cheaper than Linode (Same spec server -- 2.49EUR)
   - Alex have been using it for a month without any issues.
 - Write next month's agenda
** TODO from last month
  - Alex to talk with Miles about March Python theme
  - Scott to hear back soon from Chris Tyler on February talk
  - Alex to continue pestering Hugh on hardware war stories
  - Alex to contact Stewart Russell to see if he has anything with his new place
  - Alex to set up meetup.com account
