---
layout: post
title: "Agenda"
published: true
---

* 16 December 2019 at 7:30pm

** Location
 - Starbucks, Davisville and Yonge
 - Google Hangouts as backup

** Agenda Topics
 - Timing/Location of next Ops meeting
   - Look at Hacklab again, TPL
 - Upcoming Meetings
 - ICANN matters
 - Write next month's agenda
 - IndieWeb alternative to Meetup.com https://www.jvt.me/posts/2019/11/14/ditch-event-platforms-indieweb/
   - https://marcusnoble.co.uk/2019-10-21-meetup-alternatives/
   - Already live, somewhat...  https://gettogether.community/events/3158/lightning-talks/
     - code available https://github.com/GetTogetherComm/GetTogether
     - Django, Python3
   - https://www.attendize.com/index.html and https://github.com/Attendize/Attendize

*** Slush list for talks
 - Aaron Jacobs did a talk recently at Emacsconf on pulling twitch.tv streams from Emacs, also does R stuff, BigData, https://github.com/atheriel and is from Toronto

** ToDo List
 - DONE Chris :: capture the URL with German LUG cards
 - DONE Warren to contact TPL about meeting room
 - Gord to poke a councillor to get meeting room
 - Gord and Chris go to the bank, alas, Gord was under the weather
 - Alex, poke people on talks
   - DONE :: Nick, for December
   - Tomas, for Yubikey/PAM
   - Mike Kalles on something from August
   - Sergio on Debian
   - Chris on Kubernetes
   - Miles on something
   - Jill (that did DeepFakes in August)
   - Alex should contact Alex about a talk Alex would do on Spam filtering for mailing lists
 - Alex, present CoC proposals at Dec/Jan board meeting
 - DONE Alan :: set up GitLab account, we'll add him to the GTALUG stuff, send to Alex
 - Alan, "lint" the Other Local Users Group list 
- DONE Chris :: Future Publicity - German Linux Card https://twitter.com/lughannover/status/1193985622521896961
   - things they have we do not
     - QR barcode
     - link to twitter account
     - email address
   - seems like we have more or less enough stuff
