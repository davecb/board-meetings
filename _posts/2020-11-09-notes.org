---
layout: post
title: "Notes"
published: true
---

* 9 November 2020 at 7:00pm

** Location
 - Jitsi

** Attendees
- TBD
- Present
  - Alan H.
  - Warren M.
  - Stewart R.
  - Alex V. 
  - Scott S.

- Absent
** Topics
*** The Passing of Chris Brown
   - https://www.arbormemorial.ca/capital/obituaries/christopher-bruce-browne/57436/

**** Donation
   - via the obituary the family has asked for donations in lieu of flowers to go to Christian Service Brigade Canada.
   - Stewart: GTALUG was a big part of his life, and we should signal that.
   - Alan: What amount?
   - Stewart: 50 is a good starting point.
   - Alan: That's an amount I can agree with.
   - Warren: Agreed, we should really put effort into the note attached to it. As that will be more meaningful.
   - Motion: Donate $50
     - Moved by Stewart. Passed with 4 votes in favor.
   - Guestbook will be signed with GTALUG message.

**** Account signatories
   - We were about to add another person to the signatories.
   - Scott says he still has signing authority.
     - Scott: I'm still on the bank-account. But my bankcard is expired and can't log into the account anymore.
     - Scott: I have the email address of our Bank rep from Dec. 2016.
     - Alan: Having you reaching makes most sense for now.
     - Scott: I would CC the board on everything.
     - Stewart: But who do we want on it? Gord was the plan.
     - Alex: I should be added as well?
     - Stewart: We should retain Scott as a backup.

**** Paypal
   - Does anyone else have control of this? I'm on the board of another local computer group that no longer has access to its Paypal account, and Paypal are very difficult to work with.
   - Stewart: This was a missunderstanding. GTALUG does not have Paypal.

****  Ongoing payments
   - Review Finanical report given at last months AGM.
   - Seems to be just the hosting and DNS.

**** Accounts
   - these are probably rather well kept, but we may not know where.
   - Linode account (Hosting)
   - Gandi.net account (DNS) for gtalug.org and gtalug.ca

**** Transition
   - we'll need another board member, and reallocate office-bearers.
     - Previous elections were 'sparse'.
     - Alan: Our situation doesn't help, and we're not getting good participation currently.
     - Stewart: Agree on filling the positions / roles.
     - Stewart: Review of by-laws shows that 10(b) allows the board to fill the vacacy by vote of the directors.
   - Call a By-Election for the remainder of Chirs's Term.
     - Not needed as per section 10(b) of bylaws.
   - Motion: Appoint Scott to the Board
     - Motion brought by Stewart. Passed with 4 votes in Favor.


**** Are there any SOPs in the technical infra?
   - Git Repos
     - Add Stewart and Alan.
   - Server
     - Current access from Scott and Alex is sufficent.
   - Email
     - Confirm Stewart and Scott are on Board list.


*** Members Meeting for the 10th.
  - Stewart: Were having the meeting. We should put aside a bit of time to Speak well of Chris and Remember him.
  - Alan: We have an LPI partnership presentation from Evan. 5-7min.
  - Scott: I have 20min ready about doing HA on raspberry Pis.

** TODO
- [] Scott to Contact bank and start Signing Auth updates.
- [] Alex to Contact Linode about transfering the account and services.
- [] Scott to update mailman configs for missing users.
- [] Scott to Contact Mike Hoye about December.