---
layout: post
title: "Agenda"
published: true
---

* 16 November 2020 at 7:30pm

** Location
 - Remote based on COVID-19
 - Jitsi

** Agenda Topics
 - Timing/Location of next Ops meeting
   - Look at Jitsi again
 - LPI Community partnership (Evan)
 - COVID-19 question
   - we are a discretionary meeting
 - Possible alternative venue after COVID-19 offered by David CB. 
    - We're at 468 King West, at Spadina and just around the corner from Spotify 
    - https://www.openstreetmap.org/node/2554589662#map=17/43.64555/-79.39593
 - Upcoming Meetings
   - Chris pleads for more idea material
   - Quite a number of the October items attracted near-zero interest
 - ICANN matters
 - Write next month's agenda
   - Chris to do this offline
** TODO
 - [ ] Update _data/members.yml after board election, each year
 - [-] Alex [1/3]
   - [ ] Contacting Mike Hoye about possibly presenting in November or December
   - [ ] Publish the Ansible rules for spinning up Jitsi
 - [ ] Alan
   - [ ] Submit a brief presentation on implications of the LPI Community Partnership
   - [ ] Present at December meeting
 - [ ] Chris
   - [ ] Ensure that we have record that we need one of this year's
     Board members to end their term October 2021
   - [ ] Visit Spadina/College CIBC to see requirements for Evan and
     Gord to become cosigners
 - [ ] Stewart
   - [ ] Trevor Woerner (OpenEmbedded - he spoke before, yes?)
     recently gave a great talk on building embedded Linuxes if you
     wanted another speaker, Trevor Woerner (OpenEmbedded - he spoke
     before, yes?)  recently gave a great talk on building embedded
     Linuxes
